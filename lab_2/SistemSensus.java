import java.util.Locale;
import java.util.Scanner;

/**
 * @author Template Author: Ichlasul Affan dan Arga Ghulam Ahmad
 * Template ini digunakan untuk Tutorial 02 DDP2 Semester Genap 2017/2018.
 * Anda sangat disarankan untuk menggunakan template ini.
 * Namun Anda diperbolehkan untuk menambahkan hal lain berdasarkan kreativitas Anda
 * selama tidak bertentangan dengan ketentuan soal.
 *
 * Cara penggunaan template ini adalah:
 * 1. Isi bagian kosong yang ditandai dengan komentar dengan kata TODO
 * 2. Ganti titik-titik yang ada pada template agar program dapat berjalan dengan baik.
 *
 * Code Author (Mahasiswa):
 * @author Kevin Raikhan Zain, NPM 1706075041, Kelas DDP-F, GitLab Account: kevinrz123
 */

public class SistemSensus {
	public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in).useLocale(Locale.US);


		// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
		// User Interface untuk meminta masukan
		System.out.print("PROGRAM PENCETAK DATA SENSUS\n" +
				"--------------------\n" +
				"Nama Kepala Keluarga   : ");
		for (int a = 0; a < 1;a++)
		{

			String nama = input.nextLine();
			System.out.print("Alamat Rumah           : ");
			String alamat = input.nextLine();

			System.out.print("Panjang Tubuh (cm)     : ");
			double panjang = input.nextDouble();

			if (panjang < 0 || panjang >= 250) {
				System.out.println("Warning : Tidak Perlu Relokasi");
				break;
			}


			System.out.print("Lebar Tubuh (cm)       : ");
			double lebar = input.nextDouble();
			if (lebar < 0 || lebar >= 250) {
				System.out.println("Warning : Tidak Perlu Relokasi");
				break;
			}



			System.out.print("Tinggi Tubuh (cm)      : ");
			double tinggi = input.nextInt();
			if (tinggi < 0 || tinggi >= 250) {
				System.out.println("Warning : Tidak Perlu Relokasi");
				break;
			}


			System.out.print("Berat Tubuh (kg)       : ");
			double berat = input.nextDouble();
			if (berat < 0 || berat >= 150) {
				System.out.println("Warning : Tidak Perlu Relokasi");
				break;
			}


			System.out.print("Jumlah Anggota Keluarga: ");
			int makanan = input.nextInt();

			if (makanan < 0 || makanan >= 20) {
				System.out.println("Warning : Tidak Perlu Relokasi");
				break;
			}

			input.nextLine();

			System.out.print("Tanggal Lahir          : ");
			String tanggalLahir = input.nextLine();

			System.out.print("Catatan Tambahan       : ");
			String catatan = input.nextLine();

			System.out.print("Jumlah Cetakan Data    : ");
			int jumlahCetakan = input.nextInt();
			input.nextLine();

			// TODO Bagian ini digunakan untuk soal Tutorial "Sensus Daerah Kumuh"
			// TODO Hitung rasio berat per volume (rumus lihat soal)


			double vol = (panjang * lebar * tinggi);
			double rasio = (berat / (vol/1000000));

			int rasio_int = (int) rasio;


			// TODO Periksa ada catatan atau tidak
			if (catatan.length() > 0) {
				catatan = String.format("Catatan : %s",catatan );
			} else {
				catatan = "Tidak Ada Catatan";
			}

			for (int i = 1; i < jumlahCetakan + 1; i++) {
				// TODO Minta masukan terkait nama penerima hasil cetak data
				System.out.print("\nPencetakan " + i + " dari " + jumlahCetakan + " untuk: ");
				String penerima = input.nextLine().toUpperCase(); // Lakukan baca input lalu langsung jadikan uppercase



				// TODO Cetak hasil (ganti string kosong agar keluaran sesuai)
				System.out.println("DATA SIAP DICETAK UNTUK " + penerima + "\n ---------------------------------");
				System.out.println(nama + " - " + alamat);
				System.out.println("Lahir Pada Tanggal " + tanggalLahir);
				System.out.println("Rasio Berat per Volume = " + rasio_int + " Kg/m^3");
				System.out.println(catatan);
			}




			// TODO Bagian ini digunakan untuk soal bonus "Rekomendasi Apartemen"
			// TODO Hitung nomor keluarga dari parameter yang telah disediakan (rumus lihat soal)

			char[] nama_array = nama.toCharArray();

			int jumlah_asc = 0;
			for (char i:nama_array){
				jumlah_asc += (int) i;
			}

			int vol_int = (int) vol;

			int kalkul = (jumlah_asc + vol_int);
			String kalkul_str = String.valueOf(kalkul);


			// TODO Gabungkan hasil perhitungan sesuai format sehingga membentuk nomor keluarga
		String nomorKeluarga = nama.charAt(0) + kalkul_str.substring(kalkul_str.length()-4);

//
		// TODO Hitung anggaran makanan per tahun (rumus lihat soal)
		int anggaran = (makanan * 50000 * 365);
//
		// TODO Hitung umur dari tanggalLahir (rumus lihat soal)
		String[] tahunLahir_array = tanggalLahir.split("-"); // lihat hint jika bingung
			int tahunLahir = Integer.parseInt(tahunLahir_array[2]);
			int umur = 2018 - tahunLahir;


//
//		// TODO Lakukan proses menentukan apartemen (kriteria lihat soal)
		String tempat,kabupaten;
		if(umur <= 18){
			tempat = "PPMT";
			kabupaten = "Rotunda";
		}else{
			if (anggaran < 100000000){
				tempat = "Teksas";
				kabupaten = "Sastra";
			}else {
				tempat = "Mares";
				kabupaten = "Margonda";
			}
		}




//		// TODO Cetak rekomendasi (ganti string kosong agar keluaran sesuai)
		System.out.println("\nREKOMENDASI APARTEMEN\n------------------------");
		System.out.println("MENGETAHUI : " + nama + " - " + nomorKeluarga);
			System.out.println("MENIMBANG: Anggaran makanan tahunan: Rp " +anggaran +"\n           Umur kepala keluarga: "
					+ umur);
			System.out.println("MEMUTUSKAN: keluarga " + nama + " akan ditempatkan di: " + tempat + ", Kabupaten " + kabupaten);

		input.close();

		}

	}
}