import java.util.*;
import java.io.*;

abstract class Karyawan {

  private double gaji;
  private String nama;
  private int lamaKerja;

  public Karyawan(String nama, double gaji) {
    this.nama = nama;
    this.gaji = gaji;
    this.lamaKerja = 0;
  }

  public abstract void tambahBawahan(Karyawan bawahan);

  public void tambahLamaKerja() {
    this.lamaKerja++;
  }

  public String getNama() {
    return this.nama;
  }

  public void setNama(String nama) {
    this.nama = nama;
  }

  public double getGaji() {
    return this.gaji;
  }

  public void setGaji(double gaji) {
    this.gaji = gaji;
  }

  public int getLamaKerja() {
    return this.lamaKerja;
  }

  public void setLamaKerja(int lamaKerja) {
    this.lamaKerja = lamaKerja;
  }
}

class Staff extends Karyawan {

  private List<Karyawan> bawahan;

  public Staff(String nama, double gaji) {
    super(nama, gaji);
    this.bawahan = new ArrayList<>();
  }

  public void tambahBawahan(Karyawan bawahanBaru) {
    if (bawahanBaru instanceof Staff) {
      System.out.println("Anda tidak layak memiliki bawahan");
    }
    for (Karyawan k : this.bawahan) {
      if (k.getNama().equals(bawahanBaru.getNama())) {
        System.out.printf("Karyawan %s telah menjadi bawahan %s\n", bawahanBaru.getNama(), this.getNama());
        return;
      }
    }
    this.bawahan.add(bawahanBaru);
    System.out.printf("Karyawan %s berhasil ditambahkan menjadi bawahan %s\n", bawahanBaru.getNama(), this.getNama());
  }

  public List<Karyawan> getBawahan() {
    return this.bawahan;
  }

  public void setBawahan(List<Karyawan> bawahan) {
    this.bawahan = bawahan;
  }

}

class Manager extends Staff {

  public Manager(String nama, double gaji) {
    super(nama, gaji);
  }

  public void tambahBawahan(Karyawan bawahanBaru) {
    for (Karyawan k : this.getBawahan()) {
      if (k.getNama().equals(bawahanBaru.getNama())) {
        System.out.printf("Karyawan %s telah menjadi bawahan %s\n", bawahanBaru.getNama(), this.getNama());
        return;
      }
    }
    this.getBawahan().add(bawahanBaru);
    System.out.printf("Karyawan %s berhasil ditambahkan menjadi bawahan %s\n", bawahanBaru.getNama(), this.getNama());
  }

}

class Intern extends Karyawan {

  public Intern(String nama, double gaji) {
    super(nama, gaji);
  }

  public void tambahBawahan(Karyawan bawahanBaru) {
    System.out.println("Anda tidak layak memiliki bawahan");
  }
}

class Korporasi {

  List<Karyawan> karyawan;
  double batasAtasGajiStaff;

  public Korporasi(double batasAtasGajiStaff) {
    karyawan = new ArrayList<>();
    this.batasAtasGajiStaff = batasAtasGajiStaff;
  }

  public void gajian() {
    int index = 0;
    for (Karyawan k : this.karyawan) {
      k.tambahLamaKerja();
      if (k.getLamaKerja() % 6 == 0 && k.getLamaKerja() != 0) {
        double gajiSebelum = k.getGaji();
        k.setGaji(k.getGaji() + k.getGaji() * 0.1);
        double gajiSesudah = k.getGaji();
        System.out.printf("%s mengalami kenaikan gaji sebesar 10%% dari %.0f menjadi %.0f\n", k.getNama(), gajiSebelum, gajiSesudah);
        if (k.getGaji() >= this.batasAtasGajiStaff && k.getClass() == Staff.class) {
          Manager managerBaru = new Manager(k.getNama(), k.getGaji()); // dari staff jadi manager
          this.karyawan.set(index, managerBaru);
          this.keluarkanDariBawahan(managerBaru); // manager tidak boleh jadi bawahan
          System.out.printf("Selamat, %s telah dipromosikan menjadi MANAGER\n", k.getNama());
        }
      }
      index++;
    }
    System.out.println("Semua karyawan sudah diberikan gaji");
  }

  public void keluarkanDariBawahan(Karyawan managerBaru) {
    for (Karyawan k : this.karyawan) {
      if (k.getClass() == Intern.class) continue;
      Manager karyawanManagerBaru = (Manager) k;
      Iterator <Karyawan> i = karyawanManagerBaru.getBawahan().iterator();
      while (i.hasNext()) {
        Karyawan bawahan = i.next();
        if (bawahan.getNama().equals(managerBaru.getNama())) {
          i.remove();
        }
      }
    }
  }

  public void tambahKaryawan(Karyawan karyawanBaru) {
    boolean sudahAda = false;
    for (Karyawan k : this.karyawan) {
      if (k.getNama().equals(karyawanBaru.getNama())) {
        System.out.printf("Karyawan dengan nama %s telah terdaftar\n", k.getNama());
        return;
      }
    }

    this.karyawan.add(karyawanBaru);
    String posisi;
    if (karyawanBaru instanceof Intern) {
      posisi = "INTERN";
    } else if (karyawanBaru instanceof Manager) {
      posisi = "MANAGER";
    } else {
      posisi = "STAFF";
    }
    System.out.printf("%s mulai bekerja sebagai %s di PT. TAMPAN\n", karyawanBaru.getNama(), posisi);
  }

  public Karyawan getKaryawan(String nama) {
    for (Karyawan k : this.karyawan) {
      if (k.getNama().equals(nama)) {
        return k;
      }
    }
    return null;
  }

  public void cetakInformasi(String nama) {
    for (Karyawan k : this.karyawan) {
      if (k.getNama().equals(nama)) {
        System.out.printf("%s %.0f\n", k.getNama(), k.getGaji());
        return;
      }
    }
    System.out.println("Karyawan tidak ditemukan");
  }
}

public class Solution {

  public static void main(String[] args) throws IOException {
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    // baca batas atas gaji staff
    double batasAtasGajiStaff = Double.parseDouble(in.readLine());
    Korporasi tampan = new Korporasi(batasAtasGajiStaff);

    // baca sampai akhir input
    String input = null;
    while ((input = in.readLine()) != null) {
      String[] inputSplitted = input.split(" ");
      if (inputSplitted[0].equals("TAMBAH_KARYAWAN")) {
        String nama = inputSplitted[1];
        double gaji = Double.parseDouble(inputSplitted[3]);

        Karyawan karyawanBaru;
        if (inputSplitted[2].equals("INTERN")) {
          karyawanBaru = new Intern(nama, gaji);
        } else if (inputSplitted[2].equals("STAFF")) {
          karyawanBaru = new Staff(nama, gaji);
        } else {
          karyawanBaru = new Manager(nama, gaji);
        }
        tampan.tambahKaryawan(karyawanBaru);
      } else if (inputSplitted[0].equals("STATUS")) {
        String nama = inputSplitted[1];
        tampan.cetakInformasi(nama);
      } else if (inputSplitted[0].equals("TAMBAH_BAWAHAN")) {
        Karyawan atasan = tampan.getKaryawan(inputSplitted[1]);
        Karyawan bawahan = tampan.getKaryawan(inputSplitted[2]);

        if (atasan == null || bawahan == null) {
          System.out.println("Karyawan tidak ditemukan");
        } else {
          atasan.tambahBawahan(bawahan);
        }
      } else {
        tampan.gajian();
      }
    }
  }

}
