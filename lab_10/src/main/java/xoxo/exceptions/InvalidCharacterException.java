package xoxo.exceptions;

import javax.swing.*;

/**
 * Exception jika kiss key mengandung selain uppercase, lowercase, dan @
 */
public class InvalidCharacterException extends RuntimeException {

    //TODO: Implement the exception


    public InvalidCharacterException(String message) {
        super(message);
        JOptionPane.showMessageDialog(null, "String Kiss Key hanya boleh mengandung huruf A-Z, a-z, dan karakter @");
    }
}