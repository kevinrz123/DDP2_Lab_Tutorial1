package xoxo.exceptions;

import javax.swing.*;

/**
 * Exception jika seed tidak 0 - 36
 */

public class RangeExceededException extends RuntimeException {


    //TODO: Implement the exception

    public RangeExceededException(String message) {
        super(message);
        JOptionPane.showMessageDialog(null, "Seed hanya boleh angka di antara 0-36 (inclusive)");
    }
}