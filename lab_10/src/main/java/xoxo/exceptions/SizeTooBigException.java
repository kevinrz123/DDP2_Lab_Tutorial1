package xoxo.exceptions;

import javax.swing.*;

/**
 * Exception jika message melebihi 10 kilobit
 */
public class SizeTooBigException extends RuntimeException {

    //TODO: Implement the exception
    public SizeTooBigException(String message) {
        super(message);
        JOptionPane.showMessageDialog(null, "Message terlalu besar");
    }
}