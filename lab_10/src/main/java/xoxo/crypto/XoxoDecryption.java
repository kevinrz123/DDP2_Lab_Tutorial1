package xoxo.crypto;

import java.util.ArrayList;

/**
 * This class is used to create a decryption instance
 * that can be used to decrypt an encrypted message.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 */
public class XoxoDecryption {

    private char[] hugKeyCharArray;

    /**
     * A Hug Key string that is required to decrypt the message.
     */
    private String hugKeyString;

    /**
     * Class constructor with the given Hug Key string.
     */
    public XoxoDecryption(String hugKeyString) {
        this.hugKeyString = hugKeyString;
        hugKeyCharArray = hugKeyString.toCharArray();
    }

    /**
     * Decrypts an encrypted message.
     *
     * @param encryptedMessage An encrypted message that wants to be decrypted.
     * @return The original message before it is encrypted.
     */
    public String decrypt(String encryptedMessage, int seed) {
        //TODO: Implement decryption algorithm
        char huruf;
        char[] encryptedChar = encryptedMessage.toCharArray();
        int nilai = 0;
        ArrayList<Character> hasil = new ArrayList<Character>();

        for (int i = 0; i < encryptedMessage.length(); i++) {
            nilai = (hugKeyCharArray[i % hugKeyString.length()] ^ seed) - 'a';
            huruf = (char) (encryptedChar[i] ^ nilai);
            hasil.add(huruf);
        }


        String hasilString = "";
        for (char i : hasil) {
            hasilString += i;
        }


        return hasilString;
    }
}