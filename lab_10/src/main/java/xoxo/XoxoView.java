package xoxo;

import xoxo.crypto.XoxoDecryption;
import xoxo.crypto.XoxoEncryption;
import xoxo.exceptions.InvalidCharacterException;
import xoxo.exceptions.KeyTooLongException;
import xoxo.exceptions.RangeExceededException;
import xoxo.exceptions.SizeTooBigException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This class handles most of the GUI construction.
 *
 * @author M. Ghautsul Azham
 * @author Mgs. Muhammad Thoyib Antarnusa
 * @author <write your name here>
 */
public class XoxoView {

    /**
     * Constraints untuk layout GridBagLayout
     */
    GridBagConstraints gbc = new GridBagConstraints();


    /**
     * A field that used to be the input of the
     * message that wants to be encrypted/decrypted.
     */
    private JTextField messageField;

    /**
     * A field that used to be the input of the key string.
     * It is a Kiss Key if it is used as the encryption.
     * It is a Hug Key if it is used as the decryption.
     */
    private JTextField keyField;


    /**
     * A field to be the input of the seed.
     */
    private JTextField seedField;

    /**
     * A field that used to display any log information such
     * as you click the button, an output file succesfully
     * created, etc.
     */
    private JTextArea logField;

    /**
     * A button that when it is clicked, it encrypts the message.
     */
    private JButton encryptButton;

    /**
     * A button that when it is clicked, it decrpyts the message.
     */
    private JButton decryptButton;

    //TODO: You may add more components here

    /**
     * Class constructor that initiates the GUI.
     */
    public XoxoView() {
        this.initGui();
    }

    /**
     * Constructs the GUI.
     */
    private void initGui() {
        //TODO: Construct your GUI here

        JFrame frame = new JFrame();
        JPanel panel = new JPanel();

        panel.setLayout(new GridBagLayout());

        messageField = new JTextField(12);
        gbc.gridx = 0;
        gbc.gridy = 0;
        panel.add(messageField, gbc);

        keyField = new JTextField(12);
        gbc.gridx = 1;
        gbc.gridy = 0;
        panel.add(keyField, gbc);

        seedField = new JTextField(4);
        gbc.gridx = 2;
        gbc.gridy = 0;
        panel.add(seedField, gbc);

        encryptButton = new JButton("Encrypt!");
        encryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                String pesan = getMessageText();
                int seed = Integer.parseInt(getSeedText());
                String key = getKeyText();
                XoxoEncryption encryptor = new XoxoEncryption(key);
                String hasil = "";

                if (seed == 0) {
                    hasil = encryptor.encrypt(pesan).getEncryptedMessage();
                } else {
                    hasil = encryptor.encrypt(pesan, seed).getEncryptedMessage();
                }
                appendLog(hasil);
            }
        });
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        panel.add(encryptButton, gbc);

        decryptButton = new JButton("Decrypt!");
        gbc.gridx = 1;
        gbc.gridy = 1;
        decryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String key = getKeyText();
                String hasil = "";
                String pesan = getMessageText();
                int seed = Integer.parseInt(getSeedText());
                XoxoDecryption decryptor = new XoxoDecryption(key);

                hasil = decryptor.decrypt(pesan, seed);
                appendLog(hasil);
            }
        });
        panel.add(decryptButton, gbc);

        logField = new JTextArea(20, 28);
        logField.setEditable(false);
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 3;
        panel.add(logField, gbc);

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.add(panel);
        frame.setTitle("Encryptor");
        frame.setVisible(true);
        frame.pack();


    }

    /**
     * Gets the message from the message field.
     *
     * @return The input message string.
     */
    public String getMessageText() {
        if (messageField.getText().length() > 1280) {
            throw new SizeTooBigException("Message terlalu besar");
        }
        return messageField.getText();
    }

    /**
     * Gets the key text from the key field.
     *
     * @return The input key string.
     */
    public String getKeyText() {
        boolean atFlag = false;

        for (char i : keyField.getText().toCharArray()) {
            if (!(i == '@' || Character.isUpperCase(i) || Character.isLowerCase(i))) {
                atFlag = true;
            }

        }


        if (keyField.getText().length() > 20) {
            throw new KeyTooLongException("Key Terlalu Panjang");
        }

        if (atFlag) {
            throw new InvalidCharacterException("Invalid Char");
        }

        return keyField.getText();
    }

    /**
     * Gets the seed text from the key field.
     *
     * @return The input key string.
     */
    public String getSeedText() {
        if (Integer.parseInt(seedField.getText()) >= 28) {
            throw new RangeExceededException("Seed hanya boleh angka di antara 0-36 (inclusive)");
        }
        return seedField.getText();
    }

    /**
     * Appends a log message to the log field.
     *
     * @param log The log message that wants to be
     *            appended to the log field.
     */
    public void appendLog(String log) {
        logField.append(log + '\n');
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to encrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to encrypt a message.
     */
    public void setEncryptFunction(ActionListener listener) {
        encryptButton.addActionListener(listener);
    }

    /**
     * Sets an ActionListener object that contains
     * the logic to decrypt the message.
     *
     * @param listener An ActionListener that has the logic
     *                 to decrypt a message.
     */
    public void setDecryptFunction(ActionListener listener) {
        decryptButton.addActionListener(listener);
    }
}