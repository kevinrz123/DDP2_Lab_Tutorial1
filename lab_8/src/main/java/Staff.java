public class Staff extends Karyawan {

    public Staff(String name, int gaji) {
        super(name, gaji);

        System.out.println(name + " mulai bekerja sebagai KARYAWAN di PT. TAMPAN");
    }


    @Override
    public void addBawahan(Karyawan bawahan) {
        if (bawahanList.size() > 10) {
            System.out.println("Sudah memiliki 10 bawahan");
            return;
        } else if (bawahan instanceof Manager) {
            System.out.println("Tidak bisa memiliki bawahan Manager");
            return;
        } else if (bawahan instanceof Staff) {
            System.out.println("Tidak bisa memiliki bawahan Staff");
            return;
        } else if (!bawahanList.contains(bawahan)) {
            System.out.println("Karyawan " + bawahan.getName() + " telah menjadi bawahan " + getName());
            return;
        } else {

        }

        bawahanList.add(bawahan);
    }
}
