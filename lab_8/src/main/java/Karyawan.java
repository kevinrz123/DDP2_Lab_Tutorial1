import java.util.ArrayList;

abstract public class Karyawan {

    ArrayList<Karyawan> bawahanList = new ArrayList<>();
    private String name;
    private int gaji, jumlahGajian;

    public Karyawan(String name, int gaji) {
        this.name = name;
        this.gaji = gaji;
        jumlahGajian = 0;
    }

    public String getName() {
        return name;
    }

    public int getGaji() {
        return gaji;
    }

    abstract public void addBawahan(Karyawan bawahan);

    public void status() {
        System.out.println(name + " " + gaji);
    }

    public void gajian() {
        jumlahGajian++;

        if (jumlahGajian % 6 == 0) {
            naikGaji();
        }

    }

    public void naikGaji() {
        System.out.println(name + " mengalami kenaikan gaji sebesar 10% dari " + gaji + " menjadi " + (gaji + gaji / 10));
        gaji = gaji + (gaji / 10);
    }


}
