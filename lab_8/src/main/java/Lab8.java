import java.util.ArrayList;
import java.util.Scanner;

class Lab8 {
    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        ArrayList<Karyawan> dataKaryawan = new ArrayList<>();
        int jumlahGaji = 0;

        int batasGaji = Integer.parseInt(input.nextLine());

        while (true) {

            String masukan = input.nextLine();
            String[] masukanSplit = masukan.split(" ");

            if (masukan.equals("exit")) {
                break;
            }

            switch (masukanSplit[0]) {
                case "GAJIAN":
                    System.out.println("Semua karyawan telah diberikan gaji");

                    for (int i = 0; i < dataKaryawan.size(); i++) {
                        dataKaryawan.get(i).gajian();
                        if (dataKaryawan.get(i).getGaji() > batasGaji && dataKaryawan.get(i) instanceof Staff) {
                            System.out.println("Selamat, " + dataKaryawan.get(i).getName() + " telah dipromosikan menjadi MANAGER");
                            Manager promoted = new Manager(dataKaryawan.get(i).getName(), dataKaryawan.get(i).getGaji());

                            dataKaryawan.set(i, promoted);
                        }

                    }
                    jumlahGaji++;
                    break;
                case "TAMBAH_KARYAWAN":

                    String name = masukanSplit[1];
                    String tipe = masukanSplit[2];
                    int gaji = Integer.parseInt(masukanSplit[3]);

                    if (cariNama(name, dataKaryawan) != null) {
                        System.out.println("Karyawan dengan nama " + name + " telah terdaftar");
                        break;
                    }

                    if (tipe.equals("MANAGER")) {
                        dataKaryawan.add(new Manager(name, gaji));
                    } else if (tipe.equals("STAFF")) {
                        dataKaryawan.add(new Staff(name, gaji));
                    } else if (tipe.equals("INTERN")) {
                        dataKaryawan.add(new Intern(name, gaji));
                    }
                    break;

                case "STATUS":
                    if (cariNama(masukanSplit[1], dataKaryawan) == null) {
                        System.out.println("Karyawan tidak ditemukan");
                        break;
                    }

                    cariNama(masukanSplit[1], dataKaryawan).status();
                    break;

                case "TAMBAH_BAWAHAN":
                    if (cariNama(masukanSplit[1], dataKaryawan) == null) {
                        System.out.println("Nama tidak berhasil ditemukan");
                        break;
                    }
                    cariNama(masukanSplit[1], dataKaryawan).addBawahan(cariNama(masukanSplit[2], dataKaryawan));
                    break;
            }
        }
    }

    public static Karyawan cariNama(String name, ArrayList<Karyawan> data) {
        for (Karyawan i : data) {
            if (i.getName().equals(name)) {
                return i;
            }
        }
        return null;
    }

}