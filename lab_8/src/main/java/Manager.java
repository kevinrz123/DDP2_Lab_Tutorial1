public class Manager extends Karyawan {

    public Manager(String name, int gaji) {
        super(name, gaji);

        System.out.println(name + " mulai bekerja sebagai MANAGER di PT. TAMPAN");

    }

    @Override
    public void addBawahan(Karyawan bawahan) {
        if (bawahanList.size() > 10) {
            System.out.println("Sudah memiliki 10 bawahan");
            return;
        } else if (bawahan instanceof Manager) {
            System.out.println("Tidak bisa memiliki bawahan manager");
            return;
        } else if (bawahanList.contains(bawahan)) {
            System.out.println("Karyawan" + bawahan.getName() + " telah menjadi bawahan " + getName());
            return;
        }
        bawahanList.add(bawahan);


    }
}
