public class Intern extends Karyawan {

    public Intern(String name, int gaji) {
        super(name, gaji);
        System.out.println(name + " mulai bekerja sebagai INTERN di PT. TAMPAN");
    }

    @Override
    public void addBawahan(Karyawan bawahan) {
        System.out.println("Anda tidak layak memiliki bawahan");
    }
}
