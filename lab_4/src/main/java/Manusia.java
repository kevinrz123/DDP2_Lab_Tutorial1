import java.lang.reflect.Array;

public class Manusia {

    private String nama;
    private int umur, uang = 50000;
    private double kebahagiaan = 50;

    // Constructors

    // Constructor TANPA uang
    public Manusia(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;

    }

    // Constructor DENGAN uang
    public Manusia(String nama, int umur, int uang) {
        this.nama = nama;
        this.umur = umur;
        this.uang = uang;

    }

    // Getters
    public String getNama(){
        return nama;
    }
    public int getUmur(){
        return umur;
    }
    public int getUang(){
        return uang;
    }
    public double getKebahagiaan(){
        return kebahagiaan;
    }

    // Setters
    public void setKebahagiaan(int kebahagiaan) {
        this.kebahagiaan = (float)kebahagiaan;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setUang(int uang) {
        this.uang = uang;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    // Method Sakit
    public void sakit(String namaPenyakit) {
        kebahagiaan -= namaPenyakit.length();
        System.out.println(String.format("%s terkena penyakit %s :O", nama, namaPenyakit));
        if (kebahagiaan <= 0) {
            kebahagiaan = 0;
        }
        if (kebahagiaan >= 100) {
            kebahagiaan = 100;
        }
    }

    //Method bekerja
    public void bekerja(int durasi, int bebanKerja) {
        int pendapatan = 0;
        if (umur <= 18) {
            System.out.println(nama + " belum boleh bekerja karena masih dibawah umur D:");
        } else {
            int BebanKerjaTotal = durasi * bebanKerja;
            if (BebanKerjaTotal <= kebahagiaan) {
                kebahagiaan -= BebanKerjaTotal;
                pendapatan = BebanKerjaTotal * 10000;
                uang += pendapatan;
                System.out.println(nama + " bekerja full time, total pendapatan : " + pendapatan);
            } else {
                int DurasiBaru = (int) kebahagiaan / bebanKerja;
                BebanKerjaTotal = DurasiBaru * bebanKerja;
                pendapatan = (int)BebanKerjaTotal * 10000;
                uang += pendapatan;
                System.out.println(nama + " tidak bekerja secara full time karena sudah terlalu lelah, total pendapatan : " + pendapatan);
                kebahagiaan -= BebanKerjaTotal;
            }
        }
        if (kebahagiaan <= 0) {
            kebahagiaan = 0;
        }
        if (kebahagiaan >= 100) {
            kebahagiaan = 100;
        }

    }



    // Method rekreasi
    public void rekreasi(String namaTempat) {
        int Biaya = namaTempat.length() * 10000;
        if (Biaya <= uang) {
            kebahagiaan += namaTempat.length();
            uang -= Biaya;
            System.out.println(nama + String.format(" berekreasi di %s, %s senang :)", namaTempat, nama));
        } else {
            System.out.println(String.format("%s tidak mempunyai cukup uang untuk berekreasi di %s :(", nama, namaTempat));
        }
        if (kebahagiaan <= 0) {
            kebahagiaan = 0;
        }
        if (kebahagiaan >= 100) {
            kebahagiaan = 100;
        }
    }

    // Method beriUang tanpa jumlah
    public void beriUang(Manusia penerima) {
        int jumlah = 0;
        for (char i : penerima.nama.toCharArray()) {
            jumlah += (int) i;
        }
        jumlah *= 100;
        if (jumlah > uang){
            System.out.println(String.format("%s ingin memberi uang kepada %s namun tidak memiliki cukup uang :'(", nama, penerima.nama));
        }else{
            penerima.kebahagiaan += ((double) jumlah / 6000);
            kebahagiaan += ((double) jumlah / 6000);
            penerima.uang += jumlah;
            uang -= jumlah;
            System.out.println(String.format("%s memberi uang sebanyak %d kepada %s, mereka berdua senang :D", nama, jumlah, penerima.nama));
        }
        if (kebahagiaan < 0) {
            kebahagiaan = 0;
        }
        if (kebahagiaan > 100) {
            kebahagiaan = 100;
        }
        if (penerima.kebahagiaan > 100) {
            penerima.kebahagiaan = 100;
        }
        if (penerima.kebahagiaan < 0) {
            penerima.kebahagiaan = 0;
        }
        }


        // Method beriUang dengan jumlah
        public void beriUang (Manusia penerima,int jumlah){
            if (uang < jumlah) {
                System.out.println(String.format("%s ingin memberi uang kepada %s namun tidak memiliki cukup uang :'(", nama, penerima.nama));
            }else {
                penerima.kebahagiaan += ((double) jumlah / 6000);
                kebahagiaan += ((double) jumlah / 6000);
                penerima.uang += jumlah;
                uang -= jumlah;

                System.out.println(String.format("%s memberi uang sebanyak %d kepada %s, mereka berdua senang :D", nama, jumlah, penerima.nama));
                if (kebahagiaan < 0) {
                    kebahagiaan = 0;
                }
                if (kebahagiaan > 100) {
                    kebahagiaan = 100;
                }
                if (penerima.kebahagiaan > 100) {
                    penerima.kebahagiaan = 100;
                }
                if (penerima.kebahagiaan < 0) {
                    penerima.kebahagiaan = 0;
                }
            }
        }

        // Method toString
        public String toString () {
            return String.format("Nama\t\t: %s \n" +
                    "Umur\t\t: %s \n" +
                    "Uang\t\t: %s \n" +
                    "Kebahagiaan\t: %s", nama, umur, uang, kebahagiaan);
        }
}
