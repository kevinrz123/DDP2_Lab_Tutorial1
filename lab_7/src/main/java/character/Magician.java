package character;

public class Magician extends Human {

    public Magician(String name, int hp) {
        super(name, hp);
    }

    @Override
    public void attacked() {
        this.hp -= 20;
    }

    public void burn(Player target) {
        target.burned();
    }

    @Override
    public void burned() {
        this.hp -= 20;
    }

    @Override
    public String getTipe() {
        return "Magician";
    }
}

//  write Magician Class here