package character;

import java.util.ArrayList;

public class Player {
    protected String name;
    protected int hp;
    protected boolean isDead;
    protected boolean isMatang;
    protected ArrayList<Player> diet = new ArrayList<>();

    public Player(String name, int hp) {
        this.name = name;
        this.hp = hp;
        isMatang = false;
        isDead = false;
        if (hp <= 0) {
            isDead = true;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public boolean getMatang() {
        return isMatang;
    }

    public void setMatang(boolean matang) {
        isMatang = matang;
    }

    public boolean getDead() {
        return isDead;
    }

    public void setDead(boolean dead) {
        isDead = dead;
    }

    public ArrayList<Player> getDiet() {
        return diet;
    }

    public void setDiet(ArrayList<Player> diet) {
        this.diet = diet;
    }

    public String getTipe() {
        return "Player";
    }

    public void attacked() {
        this.hp -= 10;
        if (hp <= 0) {
            hp = 0;
            isDead = true;
        }
    }

    public void attack(Player target) {
        if (isDead) {
            return;
        }
        target.attacked();
    }

    public void burn(Player target) {
        if (isDead) {
            return;
        }
        target.burned();
    }

    public void burned() {
        this.hp -= 10;
        if (hp <= 0) {
            hp = 0;
            isDead = true;
            isMatang = true;
        }
    }

    public void eat(Player target) {
        this.hp += 15;
        this.diet.add(target);
    }

    public boolean canEat(Player target) {
        return true;
    }

}
