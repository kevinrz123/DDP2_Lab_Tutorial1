import java.util.Scanner;

public class BingoMain {
    public static void main (String[] args) {

        Scanner input = new Scanner(System.in);

        // Minta Input User Untuk Kartu Bingo ///////////////
       Number[][] angka = new Number[5][5];

       for (int i = 0; i < 5; i++) {
           String[] masukanArray = input.nextLine().split(" ");
           for (int j = 0;j < 5;j++) {
               angka[i][j] = new Number(Integer.parseInt(masukanArray[j]),i,j);
           }
        }
        ///////////////////////////////////

		// Bikin objek kartu bingo
        BingoCard kartu = new BingoCard(angka);

		// Minta user input untuk perintah
        while (true) {

            String userInput = input.nextLine();
            String[] userInputA = userInput.split(" ");

            if (userInputA[0].equals("MARK")) {
//                kartu.markNum(Integer.valueOf(userInputA[1]));
                System.out.println(kartu.markNum(Integer.valueOf(userInputA[1])));
            } else if (userInputA[0].equals("INFO")) {
                System.out.println(kartu.info());
            } else if (userInputA[0].equals("RESTART")) {
                kartu.restart();
            }
        }
    }
}
