package lab9.event;

import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * A class representing an event and its properties
 *
 * @author kevin
 */
public class Event {
    /**
     * Name of event
     */
    private String name;
    private Date startTime, endTime;
    private BigInteger cost;
    private GregorianCalendar calStart = new GregorianCalendar();
    private GregorianCalendar calEnd = new GregorianCalendar();
    private SimpleDateFormat formatWaktu = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
    private SimpleDateFormat printFormatWaktu = new SimpleDateFormat("dd-MM-yyy, HH:mm:ss");

    // TODO: Make instance variables for representing beginning and end time of event

    // TODO: Make instance variable for cost per hour

    // TODO: Create constructor for Event class

    /**
     * Membuat objek event dengan nama, waktu mulai, waktu selesai dan biaya
     *
     * @param name      nama
     * @param startTime waktu mulai
     * @param endTime   waktu selesai
     * @param cost      biaya
     */
    // Constructor
    public Event(String name, Date startTime, Date endTime, String cost) {
        this.name = name;
        this.cost = new BigInteger(cost);
        this.startTime = startTime;
        this.endTime = endTime;
        calStart.setTime(startTime);
        calEnd.setTime(startTime);
    }


    /**
     * Accessor for name field.
     *
     * @return name of this event instance
     */
    public String getName() {
        return this.name;
    }

    /**
     * Accessor for Cost field.
     *
     * @return cost event
     */
    public BigInteger getCost() {
        return cost;
    }

    /**
     * Accessor for startTime field.
     *
     * @return waktu mulai event
     */
    public Date getStartTime() {
        return startTime;
    }

    /**
     * Accessor for endTime field.
     *
     * @return waktu selesai event
     */
    public Date getEndTime() {
        return endTime;
    }

    // TODO: Implement toString()

    /**
     * @return
     */
    @Override
    public String toString() {
        return name + "\n"
                + "Waktu mulai: " + printFormatWaktu.format(startTime) + "\n"
                + "Waktu selesai: " + printFormatWaktu.format(endTime) + "\n"
                + "Biaya kehadiran: " + cost;

    }


    // HINT: Implement a method to test if this event overlaps with another event
    //       (e.g. boolean overlapsWith(Event other)). This may (or may not) help
    //       with other parts of the implementation.
}
