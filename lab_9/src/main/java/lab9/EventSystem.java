package lab9;

import lab9.event.Event;
import lab9.user.User;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Class representing event managing system
 *
 * @author kevin
 */
public class EventSystem {

    private SimpleDateFormat formatWaktu = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
    private Date startTime, endTime;

    /**
     * List of events
     */
    private ArrayList<Event> events;

    /**
     * List of users
     */
    private ArrayList<User> users;

    /**
     * Constructor. Initializes events and users with empty lists.
     */
    public EventSystem() {
        this.events = new ArrayList<>();
        this.users = new ArrayList<>();
    }

    /**
     * Method untuk menambahkan objek event ke system(ArrayList events)
     *
     * @param name           nama event
     * @param startTimeStr   waktu mulai
     * @param endTimeStr     waktu selesai
     * @param costPerHourStr biaya event
     * @return keterangan apakah event berhasil ditambahkan
     */
    public String addEvent(String name, String startTimeStr, String endTimeStr, String costPerHourStr) {

        // cek apakah event sudah ada
        if (getEvent(name) != null) {
            return "Event " + name + " sudah ada!";
        }

        // buat objek date untuk membandingkan
        try {
            startTime = formatWaktu.parse(startTimeStr);
            endTime = formatWaktu.parse(endTimeStr);
        } catch (Exception e) {
            System.out.println(e);
        }

        if (startTime.after(endTime) || startTime.equals(endTime)) {
            return "Waktu yang diinputkan tidak valid!";
        }

        events.add(new Event(name, startTime, endTime, costPerHourStr));
        return "Event " + name + " berhasil ditambahkan!";
    }

    /**
     * menambahkan user ke system (ArrayList Users)
     *
     * @param name nama user
     * @return keterangan apakah berhasil menambahkan user ke EventSystem atau tidak
     */
    public String addUser(String name) {
        if (getUser(name) != null) {
            return "User " + name + " sudah ada!";
        }
        users.add(new User(name));
        return "User " + name + " berhasil ditambahkan!";
    }

    /**
     * Method register event ke suatu user
     *
     * @param userName  nama user yang menghadiri
     * @param eventName nama event yang akan dihadiri
     * @return keterangan apakah user menghadiri event
     */
    public String registerToEvent(String userName, String eventName) {
        // TODO: Implement
        if (getUser(userName) == null && getEvent(eventName) == null) {
            return "Tidak ada pengguna dengan nama " + userName + " dan acara dengan nama " + eventName + "!";
        } else if (getUser(userName) == null) {
            return "Tidak ada pengguna dengan nama " + userName + "!";
        } else if (getEvent(eventName) == null) {
            return "Tidak ada acara dengan nama " + eventName + "!";
        } else if (getUser(userName).tabrakCheck(getEvent(eventName))) {
            return userName + " sibuk sehingga tidak dapat menghadiri " + eventName + "!";
        }

        getUser(userName).addEvent(getEvent(eventName));
        return userName + " berencana menghadiri " + eventName + "!";
    }


    /**
     * Method untuk mengambil user dari List users dengan parameter nama user
     *
     * @param name nama user
     * @return user yang di inginkan, return null jika tidak ada
     */
    public User getUser(String name) {
        for (User i : users) {
            if (i.getName().equals(name)) {
                return i;
            }
        }
        return null;
    }


    /**
     * Method untuk mengambil event dari List events dengan parameter nama event
     *
     * @param name nama event
     * @return event yang di inginkan, return null jika tidak ada
     */
    public Event getEvent(String name) {
        for (Event i : events) {
            if (i.getName().equals(name)) {
                return i;
            }
        }
        return null;
    }
}