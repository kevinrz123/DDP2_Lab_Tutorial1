package lab9.user;

import lab9.event.Event;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Class representing a user, willing to attend event(s)
 *
 * @author kevin
 */
public class User {
    /**
     * Name of user
     */
    private String name;

    /**
     * List of events this user plans to attend
     */
    private ArrayList<Event> events;

    /**
     * Total Cost semua event yang akan dihadiri user dengan default value 0
     */
    private BigInteger totalCost = new BigInteger("0");

    /**
     * Constructor
     * Initializes a user object with given name and empty event list
     *
     * @param name nama user
     */
    public User(String name) {
        this.name = name;
        this.events = new ArrayList<>();
    }

    /**
     * Accessor for name field
     *
     * @return name of this instance
     */
    public String getName() {
        return name;
    }

    /**
     * Adds a new event to this user's planned events, if not overlapping
     * with currently planned events.
     *
     * @param newEvent
     * @return true if the event if successfully added, false otherwise
     */
    public boolean addEvent(Event newEvent) {
        // TODO: Implement!

        if (!tabrakCheck(newEvent)) {
            totalCost = totalCost.add(newEvent.getCost());
            events.add(newEvent);
            return true;
        }
        return false;
    }

    /**
     * Returns the list of events this user plans to attend,
     * Sorted by their starting time.
     * Note: The list returned from this method is a copy of the actual
     * events field, to avoid mutation from external sources
     *
     * @return list of events this user plans to attend
     */
    public ArrayList<Event> getEvents() {
        // TODO: Implement!
        // WARNING: The list returned needs to be a copy of the actual events list.
        //          You don't want people to change your plans (e.g. clearing the
        //          list) without your consent, right?
        // HINT: see Java API Documentation on ArrayList (java.util.ArrayList)                                                                                                          (or... Google. Yeah that works too. OK.)
        ArrayList<Event> eventsClone = (ArrayList<Event>) events.clone();
        Collections.sort(eventsClone, new Comparator<Event>() {
            @Override
            public int compare(Event o1, Event o2) {
                return o1.getStartTime().compareTo(o2.getStartTime());
            }
        });
        return eventsClone;
    }

    public BigInteger getTotalCost() {
        return totalCost;
    }


    /**
     * Method helper untuk cek apakah ada event yang tabrakan
     *
     * @param event event yang akan di cek
     * @return true jika sudah ada event di events dengan waktu bertabrakan
     */
    public boolean tabrakCheck(Event event) {
        for (Event i : events) {

            // Waktu StartTime event1 diantara startTime dan Endtime event2
            if (event.getEndTime().after(i.getStartTime()) && event.getEndTime().before(i.getEndTime())) {
                return true;
            }
            // Waktu EndTime event1 diantara startTime dan Endtime event2
            else if (event.getStartTime().after(i.getStartTime()) && event.getStartTime().before(i.getEndTime())) {
                return true;
            }
            // Waktu StartTime event1 sebelum startTime event2 dan waktu endTime event1 sesudah startTime event2
            else if (event.getStartTime().before(i.getStartTime()) && event.getEndTime().after(i.getEndTime())) {
                return true;
            }
        }
        return false;
    }
}
