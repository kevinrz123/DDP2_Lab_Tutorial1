
/**
 * BingoCard Template created by Nathaniel Nicholas
 * Template untuk mengerjakan soal bonus tutorial lab 5
 * Template ini tidak wajib digunakan
 * Side Note : Jangan lupa untuk membuat class baru yang memiliki method main untuk menjalankan program dengan spesifikasi yang diharapkan
 */

public class BingoCard {

	private Number[][] numbers;

	private boolean isBingo;
	
	public BingoCard(Number[][] numbers) {
		this.numbers = numbers;
		this.isBingo = false;
	}

	public Number[][] getNumbers() {
		return numbers;
	}

	public void setNumbers(Number[][] numbers) {
		this.numbers = numbers;
	}

//	public Number[] getNumberStates() {
//		return numberStates;
//	}
//
//	public void setNumberStates(Number[] numberStates) {
//		this.numberStates = numberStates;
//	}

	public boolean isBingo() {
		return isBingo;
	}

	public void setBingo(boolean isBingo) {
		this.isBingo = isBingo;
	}

	// Method Mark Card
	public String markNum(int num){
		//TODO Implement

		for (int i = 0;i < 5;i++) {
			for (int j = 0;j < 5;j++) {

				if (numbers[i][j].getValue() == num) {
					if (numbers[i][j].isChecked()) {
//						System.out.println(String.format("%d sebelumnya sudah tersilang",numbers[i][j].getValue()));
						return String.format("%d sebelumnya sudah tersilang",numbers[i][j].getValue());

					}else {
						numbers[i][j].setChecked(true);
//						System.out.println(String.format("%d tersilang", numbers[i][j].getValue()));
						cekBingo();
						return String.format("%d tersilang", numbers[i][j].getValue());

					}
				}
			}
		}
//		System.out.println(String.format("Kartu tidak memiliki angka %d",num ));
		return String.format("Kartu tidak memiliki angka %d",num );
	}

	// Method Info
	public String info(){
		//TODO Implement
		String empty = "";

		for (Number[] i : numbers) {
			for (Number j : i) {

				empty += "| ";

				if ( j.isChecked()) {
					empty += "X  ";
				}else {
					empty += j.getValue() + " ";
				}
			}
			empty += "|\n";
		}
		return empty.substring(0,empty.length() - 1);
	}
	
	// Method Restart
	public void restart(){
		//TODO Implement
		System.out.println("Mulligan!");
		for (Number[] i : numbers) {
			for (Number j : i) {
				j.setChecked(false);
			}
		}
		isBingo = false;
	}


	// Method untuk cek jika sudah bingo atau belum
	public void cekBingo() {

		for (int i = 0;i < 5;i++) {
		// Bingo Vertical
			if (numbers[0][i].isChecked() && numbers[1][i].isChecked() && numbers[2][i].isChecked() && numbers[3][i].isChecked() && numbers[4][i].isChecked()) {
//				System.out.println("Bingo!!");
				isBingo = true;
			}
			// Bingo Horizontal
			else if (numbers[i][0].isChecked() && numbers[i][1].isChecked() && numbers[i][2].isChecked() && numbers[i][3].isChecked() && numbers[i][4].isChecked()) {
//				System.out.println("Bingo!!");
				isBingo = true;
			}
			// Bingo Diagonal 1
			else if (numbers[0][0].isChecked() && numbers[1][1].isChecked() && numbers[2][2].isChecked() && numbers[3][3].isChecked() && numbers[4][4].isChecked()) {
//				System.out.println("Bingo!!");
				isBingo = true;
			// Bingo Diagonal 2
			}else if (numbers[0][4].isChecked() && numbers[1][3].isChecked() && numbers[2][2].isChecked() && numbers[3][1].isChecked() && numbers[4][0].isChecked()) {
//				System.out.println("Bingo!!");
				isBingo = true;
			}
		}
		// Print jika Bingo
		if (isBingo) {
			System.out.println("Bingo!!");
			System.out.println(info());
		}
	}


}
